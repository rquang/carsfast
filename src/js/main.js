(function ($) {
    // Preserve URL parameters in LocalStorage
    var sid = getUrlParam('sid'),
        utm_campaign = getUrlParam('utm_campaign'),
        utm_source = getUrlParam('utm_source');
    if (sid > "") {
        // console.log(sid);
        localStorage.sid = sid;
    }
    if (utm_campaign > "") {
        // console.log(sid);
        localStorage.utm_campaign = utm_campaign;
    }
    if (utm_source > "") {
        // console.log(sid);
        localStorage.utm_source = utm_source;
    }
    function getUrlParam(name) {
        var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
        return (results && results[1]) || undefined;
    }
})(jQuery);