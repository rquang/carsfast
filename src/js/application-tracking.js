(function ($) {
    $('body').on('click', '.gform_page .gform_next_button, .gform_page .gform_button[value="Submit"]', function () {
        var $this = $(this),
            $cf_page = $(this).closest('.gform_page');
        fbq('trackCustom', 'LA_Continue', {
            page: $cf_page.attr('id').split('_')[3]
        });
        if ($cf_page.hasClass('cf-monthlybudget')) {
            fbq('trackCustom', 'LA_MonthlyBudget', {
                answer: $('input[name=input_4]:checked').val()
            });
        } else if ($cf_page.hasClass('cf-existingvehicle')) {
            fbq('trackCustom', 'LA_ExistingVehicle', {
                answer: $('input[name=input_5]:checked').val()
            });
        } else if ($cf_page.hasClass('cf-employmentstatus')) {
            fbq('trackCustom', 'LA_EmploymentStatus', {
                answer: $('input[name=input_6]:checked').val()
            });
        } else if ($cf_page.hasClass('cf-workinschool')) {
            fbq('trackCustom', 'LA_WorkInSchool', {
                answer: $('input[name=input_8]:checked').val()
            });
        } else if ($cf_page.hasClass('cf-incomedetails')) {
            fbq('trackCustom', 'LA_IncomeDetails', {
                answer: $('input[name=input_97]:checked').val()
            });
        } else if ($cf_page.hasClass('cf-incomesource')) {
            fbq('trackCustom', 'LA_IncomeSource', {
                answer: $('input[name=input_10]:checked').val()
            });
        } else if ($cf_page.hasClass('cf-annualincome')) {
            fbq('trackCustom', 'LA_AnnualIncome', {
                answer: $('input[name=input_122]').val()
            });
        } else if ($cf_page.hasClass('cf-hourlyincome')) {
            fbq('trackCustom', 'LA_HourlyIncome', {
                wage: $('input[name=input_115]').val(),
                hours: $('input[name=input_113]').val()
            });
        } else if ($cf_page.hasClass('cf-monthlyincome')) {
            fbq('trackCustom', 'LA_MonthlyIncome', {
                answer: $('input[name=input_114]').val()
            });
        } else if ($cf_page.hasClass('cf-monthlyincomebracket')) {
            fbq('trackCustom', 'LA_MonthlyIncome', {
                answer: $('input[name=input_129]:checked').val(),
            });
        } else if ($cf_page.hasClass('cf-employerlength')) {
            fbq('trackCustom', 'LA_EmployerLength', {
                years: $('input[name=input_116]').val(),
                months: $('input[name=input_117]').val()
            });
        } else if ($cf_page.hasClass('cf-employmentduration')) {
            fbq('trackCustom', 'LA_EmploymentDuration', {
                duration: $('input[name=input_130]:checked').val(),
            });
        } else if ($cf_page.hasClass('cf-employerinfo')) {
            fbq('trackCustom', 'LA_EmployerInfo', {
                company: $('input[name=input_25]').val(),
                job: $('input[name=input_26]').val()
            });
        } else if ($cf_page.hasClass('cf-address')) {
            fbq('trackCustom', 'LA_Address', {
                address: $('input[name=input_92_1]').val(),
                city: $('input[name=input_92_3]').val(),
                province: $('input[name=input_92_4]').val(),
                postal: $('input[name=input_92_5]').val()
            });
        } else if ($cf_page.hasClass('cf-location-finder')) {
            fbq('trackCustom', 'LA_VehicleSearch');
        } else if ($cf_page.hasClass('cf-residencelength')) {
            fbq('trackCustom', 'LA_ResidenceLength', {
                years: $('input[name=input_118]').val(),
                months: $('input[name=input_120]').val()
            });
        } else if ($cf_page.hasClass('cf-residencetype')) {
            fbq('trackCustom', 'LA_ResidenceType', {
                type: $('input[name=input_67]:checked').val(),
            });
        } else if ($cf_page.hasClass('cf-residenceinfo')) {
            fbq('trackCustom', 'LA_ResidenceInfo', {
                type: $('input[name=input_67]:checked').val(),
                monthly: $('input[name=input_123]').val()
            });
        } else if ($cf_page.hasClass('cf-residencecost')) {
            fbq('trackCustom', 'LA_ResidenceCost', {
                vehicle: $('input[name=input_136]:checked').val(),
            });
        } else if ($cf_page.hasClass('cf-residencelength')) {
            fbq('trackCustom', 'LA_ResidenceLength', {
                duration: $('input[name=input_67]:checked').val(),
            });
        } else if ($cf_page.hasClass('cf-dateofbirth')) {
            fbq('trackCustom', 'LA_DateOfBirth', {
                month: $('input[name=input_108]').val(),
                day: $('input[name=input_109]').val(),
                year: $('input[name=input_110]').val()
            });
        } else if ($cf_page.hasClass('cf-personalinfo')) {
            fbq('trackCustom', 'LA_PInfo', {
                fn: $('input[name=input_75]').val(),
                ln: $('input[name=input_76]').val()
            });
        } else if ($cf_page.hasClass('cf-contactinfo')) {
            fbq('trackCustom', 'LA_CInfo', {
                e: $('input[name=input_79]').val(),
                p: $('input[name=input_80]').val()
            });
        } else if ($cf_page.hasClass('cf-apptype')) {
            fbq('trackCustom', 'LA_AppType', {
                e: $('input[name=input_79]').val(),
                p: $('input[name=input_80]').val()
            });
        } else if ($cf_page.hasClass('cf-vehiclepicker')) {
            fbq('trackCustom', 'LA_VehiclePicker', {
                vehicle: $('input[name=input_137]:checked').val(),
            });
        } else if ($cf_page.hasClass('cf-vehicleinfo')) {
            fbq('trackCustom', 'LA_VInfo', {
                make: $('input[name=input_139]').val(),
                model: $('input[name=input_142]').val(),
                year: $('input[name=input_141]').val(),
                odometer: $('input[name=input_140]').val()
            });
        } else if ($cf_page.hasClass('cf-currentinterestrate')) {
            fbq('trackCustom', 'LA_CurrentInterestRate', {
                make: $('input[name=input_147]').val()
            });
        } else if ($cf_page.hasClass('cf-currentmonthlypayment')) {
            fbq('trackCustom', 'LA_CurrentMonthlyPayment', {
                make: $('input[name=input_133]').val()
            });
        } else if ($cf_page.hasClass('cf-lender')) {
            fbq('trackCustom', 'LA_Lender', {
                make: $('input[name=input_152]').val()
            });
        } else if ($cf_page.hasClass('cf-cashback')) {
            fbq('trackCustom', 'LA_Cashback', {
                vehicle: $('input[name=input_159]:checked').val(),
            });
        }
    });
})(jQuery);