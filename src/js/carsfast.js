(function ($) {
    setInterval(function () {
        // Update city in location finder
        $('.cf-location-city').html($('.address_city input').val());
        // Update the SID
        if (localStorage.sid)
            $('input[name="input_112"]').val(localStorage.sid);
        if (localStorage.utm_campaign) {
            $('input[name="input_124"]').val(localStorage.utm_campaign);
            $('input[name="input_127"]').val(localStorage.utm_campaign);
        }
        if (localStorage.utm_source) {
            $('input[name="input_125"]').val(localStorage.utm_source);
            $('input[name="input_128"]').val(localStorage.utm_source);
        }
    }, 500);
    var interval = setInterval(function () {
        if ($('.cf-location-field input[name="input_92.3"]').val()) {
            $('.cf-location-field').addClass('is__located');
            clearInterval(interval);
        }
    }, 500);
})(jQuery);