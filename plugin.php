<?php
/*
Plugin Name:    CarsFast
Plugin URI:     https://socialhero.ca
Description:    Plugin for CarsFast
Version:        1.0.0
Author:         Social Hero Media
Author URI:     https://socialhero.ca
License:        GPL-2.0+
License URI:    http://www.gnu.org/licenses/gpl-2.0.txt

This plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

This plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with This plugin. If not, see {URI to Plugin License}.
*/

if (!defined('WPINC')) {
    die;
}

// Writes to the Debug Log
if (!function_exists('write_log')) {
    function write_log($log)
    {
        if (is_array($log) || is_object($log)) {
            error_log(print_r($log, true));
        } else {
            error_log($log);
        }
    }
}

add_action('wp_enqueue_scripts', 'carsfast_enqueue_files');

function carsfast_enqueue_files() {
    wp_enqueue_style('carsfast-style', plugin_dir_url( __FILE__ ) . 'assets/css/carsfast.css');
    // wp_enqueue_script('google-maps', 'https://maps.googleapis.com/maps/api/js' , array( 'jquery' ), '', true);
    wp_enqueue_script('carsfast-script', plugin_dir_url( __FILE__ ) . 'assets/js/carsfast.min.js' , array( 'jquery','gfac-autocomplete'), '', true);
    // wp_enqueue_script('carsfast-script', plugin_dir_url( __FILE__ ) . 'src/js/carsfast.js' , array( 'jquery','gfac-autocomplete'), '', true);
    wp_enqueue_script('main-script', plugin_dir_url( __FILE__ ) . 'assets/js/main.min.js' , array( 'jquery'), '', true);
    // wp_enqueue_script('main-script', plugin_dir_url( __FILE__ ) . 'src/js/main.js' , array( 'jquery'), '', true);
    wp_enqueue_script('applicationtracking-script', plugin_dir_url( __FILE__ ) . 'assets/js/application-tracking.min.js' , array( 'jquery'), '', true);
}

add_filter( 'gform_progress_bar', 'cf_remove_steps', 10, 3 );
function cf_remove_steps( $progress_bar, $form, $confirmation_message ) {
    //if you are using the filter gform_progressbar_start_at_zero, adjust the page number as needed
    $current_page = GFFormDisplay::get_current_page( $form['id'] );
    $page_count = GFFormDisplay::get_max_page_number( $form );
    $progress_bar = str_replace( 'Step ' . $current_page . ' of ' . $page_count . ' - ', '', $progress_bar );
    return $progress_bar;
}

// Send partial entries to zap
add_action( 'gform_partialentries_post_entry_saved', 'send_to_zapier_on_partial_entry_saved', 10, 2 );
function send_to_zapier_on_partial_entry_saved( $partial_entry, $form ) {
    if ( class_exists( 'GFZapier' ) ) {
        $FirstName = rgar( $partial_entry, '75' );
        $LastName = rgar( $partial_entry, '76' );
        $Email = rgar( $partial_entry, '79' );
        if ($FirstName & $LastName & $Email)
            GFZapier::send_form_data_to_zapier( $partial_entry, $form );
    }
}

// Disable auto-complete on form.
add_filter( 'gform_form_tag', function( $form_tag ) {
    return str_replace( '>', ' autocomplete="off">', $form_tag );
   }, 11 );
// Diable auto-complete on each field.
add_filter( 'gform_field_content', function( $input ) {
    return preg_replace( '/<(input|textarea)/', '<${1} autocomplete="off" ', $input );
}, 11 );

add_action( 'gform_after_submission', 'post_to_leadcapsule', 10, 2 );
function post_to_leadcapsule( $entry, $form ) {

    // Exit if form is not an application
    if (strpos(strtolower($form['title']), 'application') != FALSE) {
        return;
    }

    $formID = $form['id'];

    // Concanenate the Month, Day, and Year
    $DOB = rgar( $entry, '108' ) . '/' . rgar( $entry, '109' ) . '/' . rgar( $entry, 110 );

    // Calculate the Monthly Gross Income
    // $MonthlyGrossIncome = ((int)rgar( $entry, '122' )/12) + ((int)(rgar( $entry, '115' ) * (int)rgar( $entry, '113' )) * 4.34524) + (int)rgar( $entry, '114' );

    // Check if using Monthly Income Bracket
    if (rgar( $entry, '129' )) {
        $MonthlyGrossIncome = rgar( $entry, '129' );
    }

    // Determine if Employed
    $employerName = rgar( $entry, '25' ) ? rgar( $entry, '25' ) : 'Not Applicable';
    $jobTitle = rgar( $entry, '26' ) ? rgar( $entry, '26' ) : 'Not Applicable';

    // Check if Admin
    if( current_user_can('administrator') ) {
        $isTest = 'True';
    } else {
        $isTest = 'False';
    }

    // Check the Form ID
    if ($formID == 13) {
        // Check Employment Duration Years/Months
        $employmentDuration = checkDuration(rgar( $entry, '130' ));
        $employmentYears = $employmentDuration['years'];
        $employmentMonths = $employmentDuration['months'];
        // Check Residence Duration Years/Months
        $residenceDuration = checkDuration(rgar( $entry, '136' ));
        $residenceYears = $residenceDuration['years'];
        $residenceMonths = $residenceDuration['months'];
        $monthlyResidenceCost = rgar( $entry, '135' );
    } else {
        $employmentYears = rgar( $entry, '116' );
        $employmentMonths = rgar( $entry, '117' );
        $residenceYears = rgar( $entry, '118' );
        $residenceMonths = rgar( $entry, '120' );
        $monthlyResidenceCost = rgar( $entry, '123' );
    }

    $endpoint_url = 'https://lar.leadcapsule.com/Leads/LeadPost.aspx';
    $body = array(
        'CampaignId' => '9E378ABF54EA79C8FB0E93060055335A',
        'IsTest' => $isTest,
        'FirstName' => preg_replace('/[^A-Za-z0-9\-]/', '', rgar( $entry, '75' )),
        'LastName' => preg_replace('/[^A-Za-z0-9\-]/', '', rgar( $entry, '76' )),
        'Address' => rgar( $entry, '92.1' ),
        'City' => rgar( $entry, '92.3' ),
        'Province' => province_abbr(rgar( $entry, '92.4' )),
        'PostalCode' => rgar( $entry, '92.5' ),
        'Phone' => preg_replace('/\D+/', '',  rgar( $entry, '80' )),
        'Email' => rgar( $entry, '79' ),
        'DateOfBirth' => $DOB,
        'EmploymentStatus' => rgar( $entry, '6' ),
        'EmployerYears' => $employmentYears,
        'EmployerMonths' => $employmentMonths,
        'EmployerName' => $employerName,
        'JobTitle' => $jobTitle,
        'ResidenceType' => rgar( $entry, '67' ),
        'ResidenceYears' => $residenceYears,
        'ResidenceMonths' => $residenceMonths,
        'MonthlyBudget' => rgar( $entry, '4' ),
        'ExistingVehicle' => rgar( $entry, '5' ),
        'MonthlyGrossIncome' => round($MonthlyGrossIncome),
        'MonthlyResidenceCost' => $monthlyResidenceCost,
        'DesiredVehicles' => rgar( $entry, '86' ),
        'SubID' => rgar( $entry, '112' ),
        'IPAddress' => rgar( $entry, 'ip' ),
        'UserAgent' => rgar( $entry, 'user_agent' )
    );

    GFCommon::log_debug( 'gform_after_submission: body => ' . print_r( $body, true ) );
    $response = wp_remote_post( $endpoint_url, array( 'body' => $body ) );
    // Send response to Slack
    // try {
    //     $responseBody = json_decode(json_encode(simplexml_load_string($response['body'])),TRUE);
    //     // write_log($responseBody);
    //     // write_log($responseBody['ResponseDetail']);
    //     slack($responseBody['ResponseDetail'] . ' - ' . $body['Email'],'#leads');
    // }
    // catch (Exception $e) {
    //     slack($e->getMessage());
    // }
    GFCommon::log_debug( 'gform_after_submission: response => ' . print_r( $response, true ) );
}

function checkDuration($duration) {
    $duration = explode(" ", $duration);
    $calculatedDuration = [ 
        'years' => 0,
        'months' => 0
    ];
    if ($duration[1] == 'years') {
        $calculatedDuration['years'] = $duration[0];
    } else {
        $calculatedDuration['months'] = $duration[0];
    }
    return $calculatedDuration;
}

function employment_check($employmentStatus) {
    $employmentTypes = [
        'Retired',
        'Student',
        'Disability',
        'Unemployed'
    ];
    if (array_key_exists($employmentStatus, $employmentTypes)) {
        return true;
    } else {
        return false;
    }
}

function province_abbr($province) {

    $provinces = [
        'Alberta' => 'AB',
        'British Columbia' => 'BC',
        'Manitoba' => 'MB',
        'New Brunswick' => 'NB',
        'Newfoundland and Labrador' => 'NL',
        'Nova Scotia' => 'NS',
        'Nunavut' => 'NU',
        'Ontario' => 'ON',
        'Prince Edward Island' => 'PEI',
        'Quebec' => 'QC',
        'Saskatchewan' => 'SK',
        'Yukon' => 'YT'
    ];
    if (array_key_exists($province, $provinces)){
        return $provinces[$province]; 
    }
}

/**
 * Send a Message to a Slack Channel.
 *
 * In order to get the API Token visit: https://api.slack.com/custom-integrations/legacy-tokens
 * The token will look something like this `xoxo-2100000415-0000000000-0000000000-ab1ab1`.
 * 
 * @param string $message The message to post into a channel.
 * @param string $channel The name of the channel prefixed with #, example #foobar
 * @return boolean
 */
function slack($message, $channel)
{
    $ch = curl_init("https://slack.com/api/chat.postMessage");
    $data = http_build_query([
        "token" => 'xoxp-594215383747-594215384867-921186999395-343c34d2ecb906eb39c170fc56105c6c',
    	"channel" => $channel, //"#mychannel",
    	"text" => $message, //"Hello, Foo-Bar channel message.",
    	"username" => "LeadCapsule Bot",
    ]);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $result = curl_exec($ch);
    curl_close($ch);
    
    return $result;
}

// Canadian Phone Number Validation
add_filter( 'gform_phone_formats', 'canada_phone_format' );
function canada_phone_format( $phone_formats ) {
    $phone_formats = array(
        'standard'      => array(
            'label'       => '(###) ###-####',
            'mask'        => '(999) 999-9999',
            'regex'       => '/^\D?([2-9]{1})\D?(\d{2})\D?\D?(\d{3})\D?(\d{4})$/',
            'instruction' => '(###) ###-####',
        )
    );
 
    return $phone_formats;
}

/**
 * getRandomWeightedElement()
 * Utility function for getting random values with weighting.
 * Pass in an associative array, such as array('A'=>5, 'B'=>45, 'C'=>50)
 * An array like this means that "A" has a 5% chance of being selected, "B" 45%, and "C" 50%.
 * The return value is the array key, A, B, or C in this case.  Note that the values assigned
 * do not have to be percentages.  The values are simply relative to each other.  If one value
 * weight was 2, and the other weight of 1, the value with the weight of 2 has about a 66%
 * chance of being selected.  Also note that weights should be integers.
 * 
 * @param array $weightedValues
 */
function getRandomWeightedElement(array $weightedValues) {
    $rand = mt_rand(1, (int) array_sum($weightedValues));

    foreach ($weightedValues as $key => $value) {
        $rand -= $value;
        if ($rand <= 0) {
        return $key;
        }
    }
}